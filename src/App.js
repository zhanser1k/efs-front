import React, { Component, Fragment } from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from './_utils';
import { alertActions } from './_actions';
import { PrivateRoute } from './components/PrivateRoute';
import { Login }  from './components/Login';
import { SignUp } from './components/SignUp';
import Home from './components/Home'

import './App.css';


class App extends Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
    }

    render() {
        return (
            <Fragment>
                <Router history={history}>
                    <div>
                        <PrivateRoute exact path="/" component={ Home } />
                        <Route path="/login" component={ Login } />
                        <Route path="/signup" component={ SignUp } />
                    </div>
                </Router>
            </Fragment>
        );
    }
}

export default connect()(App)
