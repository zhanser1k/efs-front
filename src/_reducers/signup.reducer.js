import { userConstants } from '../_constants';

let token = !!localStorage.getItem('token');
const initialState = token ? { signedUp: true, token } : {};

export function signup(state = initialState, action) {
    switch (action.type) {
        case userConstants.SIGNUP_REQUEST:
            return {
                signingUp: true,
                token: ''
            };
        case userConstants.SIGNUP_SUCCESS:
            return {
                signedUp: true,
                token: action.token
            };
        case userConstants.SIGNUP_FAILURE:
            return {};
        default:
            return state
    }
}
