import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { alert } from './alert.reducer';
import { signup } from './signup.reducer';

const rootReducer = combineReducers({
    authentication,
    alert,
    signup
});

export default rootReducer;