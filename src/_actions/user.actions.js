import { userConstants } from '../_constants';
import { userService } from '../_services/user.service';
import { history } from '../_utils';
import { alertActions } from './';

export const userActions = {
    signUp,
    login,
    logout
};

function login(phone, password) {
    return dispatch => {
        dispatch(request({ phone }));

        userService.login(phone, password)
            .then(
                user => {
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    return dispatch => {
        dispatch(logout());
        userService.logout();
        history.push('/login');
    }
    function logout() { return { type: userConstants.LOGOUT } }
}

function signUp(user) {
    return dispatch => {
        dispatch(request({ user }));
        userService.signUp(user)
            .then(
                user => {
                    dispatch(success(user))
                    dispatch(alertActions.success(user.full_name));
                },
                error => {
                    dispatch(failure(error))
                    dispatch(alertActions.error(error));
                }
            )
    }
    function request(user) { return { type: userConstants.SIGNUP_REQUEST, user } }
    function success(user) { return { type: userConstants.SIGNUP_SUCCESS, user } }
    function failure(error) { return { type: userConstants.SIGNUP_FAILURE, error } }
}


