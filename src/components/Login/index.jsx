import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { userActions } from '../../_actions';
import {
    Form,
    Button,
    Grid,
    Segment,
    Message,
    Header,
    Input
} from 'semantic-ui-react';

import phoneValidate from '../../_utils/phone-validate';

import './Login.scss';

class Login extends Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            phone: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { phone, password } = this.state;
        const { dispatch } = this.props;
        if (phone && password) {
            dispatch(userActions.login(phone, password));
        }
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });

        if (name === 'phone') {
            this.setState({ phone: phoneValidate(e.target.value)});
        }
    }

    render() {
        const { loggingIn, alert } = this.props;
        const { phone, password, submitted } = this.state;
        return (
            <Grid className='login-wrap' centered verticalAlign='middle' columns={4}>
                <Grid.Column>
                    <Segment>
                        <Header as='h3' textAlign='center'>Авторизация</Header>
                        <Form size='large' onSubmit={this.handleSubmit} loading={ loggingIn }>
                            <Form.Input
                                error={submitted && !phone && { content: 'Введите телефон', pointing: 'below' }}
                                fluid
                                type='text'
                                children={
                                    <Input
                                        label="+7"
                                        value={this.state.phone}
                                        onChange={this.handleChange}
                                        name='phone'
                                        placeholder='(XXX) XXX-XXXX'
                                    />
                                }
                                id='form-input-phone'
                            >
                            </Form.Input>
                            <Form.Input
                                error={submitted && !password && { content: 'Введите пароль', pointing: 'below' }}
                                fluid
                                icon="lock"
                                iconPosition="left"
                                type='password'
                                placeholder='Введите пароль'
                                value={this.state.password}
                                name='password'
                                onChange={this.handleChange}
                                id='form-input-password'
                            >
                            </Form.Input>
                            { alert.message && alert.type === 'negative' &&
                            <Message negative size='tiny'>
                                <Message.Header>Ошибка</Message.Header>
                                <p>{alert.message}</p>
                            </Message>
                            }
                            <Button color='blue' fluid size='large' type='submit'>Войти</Button>
                        </Form>
                    </Segment>
                    <Segment textAlign='center'>
                        <p>Еще не зарегистрированы? <Link to='/signup'>Зарегистрироваться</Link></p>
                    </Segment>
                </Grid.Column>
            </Grid>

        )
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { alert } = state;
    return {
        loggingIn,
        alert
    };
}

const connectedLoginPage = connect(mapStateToProps)(Login);
export { connectedLoginPage as Login };