import React, { Component, Fragment } from 'react';
import {
    Menu,
    Container,
    Dropdown,
    Button,
    Tab
} from 'semantic-ui-react';
import { userActions } from '../../_actions';
import { connect } from 'react-redux';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            panes: [
                {
                    menuItem: 'Подать заявку',
                    render: () => <Tab.Pane attached={false}>Подать заявку</Tab.Pane>,
                },
                {
                    menuItem: 'Калькулятор',
                    render: () => <Tab.Pane attached={false}>Калькулятор</Tab.Pane>,
                },
                {
                    menuItem: 'Заключить контракт',
                    render: () => <Tab.Pane attached={false}>Заключить контракт</Tab.Pane>,
                },
                {
                    menuItem: 'Бухгалтерия',
                    render: () => <Tab.Pane attached={false}>Бухгалтерия</Tab.Pane>,
                },
                {
                    menuItem: 'БИН/ТОО',
                    render: () => <Tab.Pane attached={false}>БИН/ТОО</Tab.Pane>,
                }
            ]
        }
        this.handleItemClick = this.handleItemClick.bind(this)
    }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    handleLogoutClick = () => {
        const { dispatch } = this.props;
        dispatch(userActions.logout());
    }

    render() {
        const { activeItem, panes} = this.state
        return (
            <Fragment>
                <Menu size='tiny'>
                    <Menu.Item
                        name='Главная'
                        active={activeItem === 'Главная'}
                        onClick={this.handleItemClick}
                    />
                    <Menu.Item
                        name='редактировать профиль'
                        active={activeItem === 'редактировать профиль'}
                        onClick={this.handleItemClick}
                    />

                    <Menu.Menu position='right'>
                        <Dropdown item text='Language'>
                            <Dropdown.Menu>
                                <Dropdown.Item>English</Dropdown.Item>
                                <Dropdown.Item>Russian</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                        <Menu.Item>
                            <Button onClick={this.handleLogoutClick} primary>Выйти</Button>
                        </Menu.Item>
                    </Menu.Menu>
                </Menu>
                <Container>
                    <Tab menu={{ attached: false, color: 'blue', tabular: false, fluid: true }} panes={panes} />
                </Container>
            </Fragment>
        );
    }
}

export default connect()(Home)