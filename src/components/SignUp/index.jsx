import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { userActions } from '../../_actions';
import {
    Form,
    Button,
    Grid,
    Segment,
    Message,
    Header, Input
} from 'semantic-ui-react';

import './Signup.scss';
import phoneValidate from "../../_utils/phone-validate";

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullName: '',
            phone: '',
            email: '',
            password: '',
            submitted: false,
            emailValid: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { fullName, phone, email, password } = this.state;
        const { dispatch } = this.props;
        if (fullName && phone && email && password) {
            dispatch(userActions.signUp(this.state));
        }
    }

    handleChange(e) {
        let { name, value } = e.target;
        this.setState({ [name]: value }, () => {
            this.validateEmailField(name, value)
        });

        if (name === 'phone') {
            this.setState({ phone: phoneValidate(e.target.value)});
        }
    }

    validateEmailField(fieldName, value) {
        let emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);

        this.setState({
            emailValid
        });
    }

    render() {
        const { signingUp, alert } = this.props;
        const { fullName, phone, email, password, submitted, emailValid } = this.state;
        return (
            <Grid className='signup-wrap' centered verticalAlign='middle' columns={4}>
                <Grid.Column>
                    <Segment>
                        <Header as='h3' textAlign='center'>Регистрация</Header>
                        <Form size='large' onSubmit={this.handleSubmit} loading={ signingUp }>
                            <Form.Input
                                error={submitted && !fullName && { content: 'Введите имя', pointing: 'below' }}
                                fluid
                                type='text'
                                placeholder='John Doe'
                                icon="user"
                                iconPosition="left"
                                value={this.state.fullName}
                                name='fullName'
                                onChange={this.handleChange}
                                id='form-input-full-name'
                            >
                            </Form.Input>
                            <Form.Input
                                error={
                                    (submitted && !email && { content: 'Введите email', pointing: 'below' })
                                    || (submitted &&!emailValid && { content: 'Неправильный формат email', pointing: 'below' })
                                }
                                fluid
                                type='email'
                                placeholder='mail@example.com'
                                icon="mail"
                                iconPosition="left"
                                value={this.state.email}
                                name='email'
                                onChange={this.handleChange}
                                id='form-input-email'
                            >
                            </Form.Input>
                            <Form.Input
                                error={submitted && !phone && { content: 'Введите телефон', pointing: 'below' }}
                                fluid
                                type='text'
                                children={
                                    <Input
                                        label="+7"
                                        value={this.state.phone}
                                        onChange={this.handleChange}
                                        name='phone'
                                        placeholder='(XXX) XXX-XXXX'
                                    />
                                }
                                id='form-input-phone'
                            >
                            </Form.Input>
                            <Form.Input
                                error={submitted && !password && { content: 'Введите пароль', pointing: 'below' }}
                                fluid
                                icon="lock"
                                iconPosition="left"
                                type='password'
                                placeholder='Введите пароль'
                                value={this.state.password}
                                name='password'
                                onChange={this.handleChange}
                                id='form-input-password'
                            >
                            </Form.Input>
                            { alert.message && alert.type === 'negative' &&
                            <Message negative size='tiny'>
                                <Message.Header>Ошибка</Message.Header>
                                <p>{alert.message}</p>
                            </Message>
                            }
                            { alert.message && alert.type === 'success' &&
                            <Message success size='tiny'>
                                <Message.Header>Поздарвляем</Message.Header>
                                <p>Уважаемый {alert.message} Регистрация прошла успешно! <Link to='/login'>Войдите</Link> используя телефон и пароль</p>
                            </Message>
                            }
                            <Button color='blue' fluid size='large' type='submit'>Зарегистрироваться</Button>
                        </Form>
                    </Segment>
                    <Segment textAlign='center'>
                        <p>Уже зарегистрированы? <Link to='/login'>Войдите</Link></p>
                    </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    const { signingUp } = state.signup;
    const { alert } = state;
    return {
        signingUp,
        alert
    };
}

const connectedLoginPage = connect(mapStateToProps)(SignUp);
export { connectedLoginPage as SignUp };