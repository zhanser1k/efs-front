import { API } from '../_utils';

export const userService = {
    signUp,
    logout,
    login
};

function errorHandler(error) {
    if (error.response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        window.location.reload(true);
    }
    const errorMessage = (error.response.data && error.response.data.detail) || error.response.statusText;
    return Promise.reject(errorMessage)
}

function login(phone, password) {
    return API.post("v1/auth/signin/", {
        phone: `+7 ${phone.replace(/[^0-9.]+/g, '')}`,
        password
    })
        .then(
            response => {
                if (response.data.token) {
                    localStorage.setItem('token', response.data.token);
                }
                return response.data.user;
            },
            error => {
                return errorHandler(error);
            }
        );
}

function signUp(user) {
    return API.post("v1/auth/signup/", {
        full_name: user.fullName,
        password: user.password,
        phone: `+7 ${user.phone.replace(/[^0-9.]+/g, '')}`,
        email: user.email
    })
        .then(
            response => {
                return response.data.user;
            },
            error => {
                return errorHandler(error)
            }
        );
}

function logout() {
    localStorage.removeItem('token');
}
